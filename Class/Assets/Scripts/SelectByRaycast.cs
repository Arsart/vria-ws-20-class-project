﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HTC.UnityPlugin.Vive;

public class SelectByRaycast : MonoBehaviour
{
    public Transform rayTransform;
    public float raycastDistance = 10;

    private LineRenderer lineRenderer;

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    public void Update()
    {
        Ray ray = new Ray(rayTransform.position, rayTransform.forward);
        RaycastHit hit;
        lineRenderer.positionCount = 2;
        lineRenderer.SetPosition(0,rayTransform.position);
        if(ViveInput.GetPressDown(HandRole.LeftHand, ControllerButton.Trigger) && Physics.Raycast(ray, out hit, raycastDistance))
        {
            ChangeColor changeColor = hit.collider.GetComponent<ChangeColor>();
            if(changeColor != null)
            {
                changeColor.SetColor(Color.red);
            }

            lineRenderer.SetPosition(1,hit.point);
        }
        else
        {
            lineRenderer.SetPosition(1, ray.origin + ray.direction * raycastDistance);
        }
    }
}
