﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Notifiable : MonoBehaviour
{
    void Notify(float distance)
    {
        transform.position = transform.position + Vector3.up * distance;
    }
}
