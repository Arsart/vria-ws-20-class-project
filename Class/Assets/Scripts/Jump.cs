﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HTC.UnityPlugin.Vive;

public class Jump : MonoBehaviour
{
    public enum Test
    {
        Default, NextOne, ThirdOne, FourthOne
    }

    public float speed;
    public float force = 10;
    public float rotateSpeed;
    public float velocityThreshold = 0.1f;
    private float currentTime;
    public float checkDistance;

    public GameObject other;
    private Rigidbody myRigidbody;
    private ChangeColor changeColor;

    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = other.GetComponent<Rigidbody>();
        changeColor = GetComponent<ChangeColor>();
    }

    private float variableDeltaTime;
    void Update() 
    {
        bool ground = IsOnGround(); 
        currentTime = Time.fixedDeltaTime;

        float horizontalAxis = ViveInput.GetAxis(HandRole.RightHand, ControllerAxis.PadX);
        float verticalAxis = ViveInput.GetAxis(HandRole.RightHand, ControllerAxis.PadY);
        Vector3 direction = new Vector3(horizontalAxis,0, verticalAxis);
        direction.Normalize();

        bool rightHandValue = ViveInput.GetPressDown(HandRole.RightHand, ControllerButton.Trigger);
        bool leftHandValue = ViveInput.GetPressDown(HandRole.LeftHand, ControllerButton.Trigger);
        bool finalValue = rightHandValue || leftHandValue;
        if(ground && finalValue)
        {
            myRigidbody.AddForce(force * Vector3.up);
        }

        if(ground)
        {
            myRigidbody.AddForce(speed * direction);
            transform.forward = direction;
            // myRigidbody.AddForce(speed * verticalAxis * transform.forward);
            // float realTorque = Mathf.Abs(verticalAxis) * horizontalAxis;
            // myRigidbody.AddTorque(transform.up * rotateSpeed * realTorque);
        }

        Color colorToApply = Color.white;

        if(!ground)
        {
            colorToApply = Color.green;
        }

        //        |
        // ___    |    ________
        //    |___|___|
        //        |
        //        |
        //Make the ball green when going up and red when comming down
        // if(myRigidbody.velocity.y > velocityThreshold)
        // {
        //     colorToApply = Color.green;
        // }
        // else if (myRigidbody.velocity.y < -velocityThreshold)
        // {
        //     colorToApply = Color.red;
        // }


        changeColor.SetColor(colorToApply);
    }

    bool IsOnGround()
    {
        return Physics.Raycast(transform.position, new Vector3(0,-1,0), transform.localScale.y * 0.6f);
    }

    void OnDrawGizmos()
    {
        RaycastHit hit;
        Physics.Raycast(transform.position, -1 * Vector3.up, out hit , transform.localScale.y*0.6f);

        if(hit.collider != null)
        {
            Gizmos.DrawLine(transform.position, hit.point);
            Gizmos.DrawSphere(hit.point, 0.1f);
        }
        else
        {
            Gizmos.DrawLine(transform.position, transform.position - Vector3.up * transform.localScale.y*0.6f);
        }

    }

}
