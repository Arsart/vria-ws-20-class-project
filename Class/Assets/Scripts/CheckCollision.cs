﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckCollision : MonoBehaviour
{
    public Text numberText;
    ChangeColor changeColor;

    void Start()
    {
        changeColor = GetComponent<ChangeColor>();
    }

    // void OnCollisionEnter(Collision collision)
    // {
    //     changeColor.SetColor(Color.green);
    // }

    // void OnCollisionExit(Collision collision)
    // {
    //     changeColor.SetColor(Color.white);
    // }

    void OnTriggerEnter(Collider collider)
    {
        changeColor.SetColor(Color.red);
        Hotspot hotspot = collider.GetComponent<Hotspot>();
        if(hotspot != null)
        {
            numberText.text = hotspot.index.ToString();
        }
    }

    void OnTriggerExit(Collider collider)
    {
        changeColor.SetColor(Color.white);
        numberText.text = "";
    }
}
