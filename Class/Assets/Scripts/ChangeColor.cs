﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    //
    MeshRenderer myRenderer;
    void Start()
    {
        myRenderer = GetComponent<MeshRenderer>();
    }

    public void SetColor(Color newColor)
    {
        myRenderer.material.color = newColor;
    }
}
