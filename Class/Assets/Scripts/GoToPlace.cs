﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToPlace : MonoBehaviour
{
    public Transform target;
    public float transitionTime = 2;

    bool isMoving = false;
    // Update is called once per frame
    void Update()
    {
        //Make object come and go back
        // float t = Mathf.PingPong(Time.time, transitionTime);
        // transform.position = Vector3.Lerp(Vector3.zero, target.position, t);

        //Go to location using coroutine
        if(!isMoving && Input.GetKeyDown(KeyCode.Space))
        {
            isMoving = true;
            //Call coroutine inside of start coroutine ()
            StartCoroutine(GoToLocation(transform.position, target.position));
        }  
    }


    // COROUTINES ARE OF RETURN TYPE IEnumerator
    IEnumerator GoToLocation(Vector3 origin, Vector3 destiny)
    {
        float counter = 0;
        float t = 0;

        while(counter < transitionTime)
        {
            transform.position = Vector3.Lerp(origin, destiny, t);
            counter += Time.deltaTime;
            t = counter / transitionTime;
            yield return null;
        }
        isMoving = false;
    }

    void Direct(Vector3 origin, Vector3 destiny)
    {
        float t = 0;

        while(t < 1)
        {
            transform.position = Vector3.Lerp(origin, destiny, t);
            t += Time.deltaTime;
            // yield return null;
        }
        isMoving = false;
    }
}
