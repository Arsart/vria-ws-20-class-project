﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationConstructor : MonoBehaviour
{
    [Header("References")]
    [Tooltip("Nodes of a graph")]
    public Node[] nodes;
    [Tooltip("Edges of a graph")]
    public Edge[] edges;

    [Header("Visualization")]
    [Tooltip("This will draw debug lines")]
    public bool drawDebugEdges = true;


    void Start()
    {
        CalculateDistances();
    }

    // To test on editor
    // public int testInitial = 0;
    // public int testFinal = 1;
    // void OnValidate()
    // {
    //     var path = GetPath(GetNode(testInitial), GetNode(testFinal));
    //     int i = 0;
    //     foreach (var item in path)
    //     {
    //         Debug.Log($"Path[{i}]:{item.index}");
    //         i++;
    //     }
    // }

    /// <summary>
    /// Calculate distances for weights of each edge
    /// </summary>
    private void CalculateDistances()
    {
        for (int i = 0; i < edges.Length; i++)
        {
            Vector3 startPosition = GetNode(edges[i].startNode).nodeObject.transform.position;
            Vector3 endPosition = GetNode(edges[i].endNode).nodeObject.transform.position;
            
            edges[i].SetDistance((startPosition - endPosition).magnitude);
        }
    }

    /// <summary>
    /// Get a node with the necessary index
    /// </summary>
    /// <param name="index">An integer with the index</param>
    /// <returns></returns>
    public Node GetNode(int index)
    {
        return nodes[index];
    }

    /// <summary>
    /// Get all the edges that lead to this node
    /// </summary>
    /// <param name="node"></param>
    /// <returns>A list with all the edges</returns>
    public List<Edge> GetIncoming(Node node)
    {
        List<Edge> result = new List<Edge>();
        for (int i = 0; i < edges.Length; i++)
        {
            if(edges[i].endNode == node.index || 
                (edges[i].bidirectional && edges[i].startNode == node.index))
            {
                result.Add(edges[i]);
            }
        }
        return result;
    }

    /// <summary>
    /// Get all the edges that this node can use
    /// </summary>
    /// <param name="node"></param>
    /// <returns>A list with all the edges</returns>
    public List<Edge> GetOutgoing(Node node)
    {
        List<Edge> result = new List<Edge>();
        for (int i = 0; i < edges.Length; i++)
        {
            if(edges[i].startNode == node.index || 
                (edges[i].bidirectional && edges[i].endNode == node.index))
            {
                result.Add(edges[i]);
            }
        }
        return result;

    }

    /// <summary>
    /// Get the node that is the closest to some position
    /// </summary>
    /// <param name="position">A world position that is near the node</param>
    /// <returns></returns>
    public Node GetNearestNode(Vector3 position)
    {
        float nearestDistance = Mathf.Infinity;
        Node nearestNode = null;
        for (int i = 0; i < nodes.Length; i++)
        {
            float distance = (nodes[i].nodeObject.transform.position - position).magnitude;
            if(distance < nearestDistance)
            {
                nearestDistance = distance;
                nearestNode = nodes[i];
            }
        }
        return nearestNode;
    }

    /// <summary>
    /// Tries to find a sequence of nodes to get to a destination
    /// </summary>
    /// <param name="intialPosition"></param>
    /// <param name="finalPosition"></param>
    /// <returns></returns>
    public Node[] GetPath(Vector3 intialPosition, Vector3 finalPosition)
    {
        Node startNode = GetNearestNode(intialPosition);
        Node endNode = GetNearestNode(finalPosition);

        return GetPath(startNode, endNode);
    }

    /// <summary>
    /// Tries to find a sequence of nodes to get to a destination
    /// </summary>
    /// <param name="intialPosition"></param>
    /// <param name="finalPosition"></param>
    /// <returns></returns>
    /// Some recomentations for finding the path
    /// https://en.wikipedia.org/wiki/Graph_traversal
    /// https://upload.wikimedia.org/wikipedia/commons/c/c7/Graph-scan.png
    public Node[] GetPath(Node intialNode, Node finalNode)
    {
        Stack<Node> path = new Stack<Node>();
        HashSet<Node> discoveredNode = new HashSet<Node>();

        FindPath(intialNode, finalNode, path, discoveredNode);

        Node[] reversedPath = path.ToArray();
        Node[] result = new Node[reversedPath.Length];
        int newIndex = 0;
        for (int i = reversedPath.Length - 1; i >= 0; i--)
        {
            result[newIndex] = reversedPath[i];
            newIndex++;
        }

        return result;
    }

    private bool FindPath(Node currentNode, Node finalNode, Stack<Node> path, HashSet<Node> discoveredNode)
    {
        if(discoveredNode.Contains(currentNode)) return false;
        discoveredNode.Add(currentNode);
        path.Push(currentNode);
        if(currentNode == finalNode)
        {
            return true;
        }
        else
        {
            foreach(var edge in GetOutgoing(currentNode))
            {
                Node nextNode;
                if(edge.endNode == currentNode.index)
                    nextNode = GetNode(edge.startNode);
                else
                    nextNode = GetNode(edge.endNode);

                bool result = FindPath(nextNode, finalNode,path, discoveredNode);
                if(result)
                {
                    return true;
                }
            }
        }
        path.Pop();
        return false;

    }

    /// <summary>
    /// Debug function to visualize the nodes inside of the editor
    /// </summary>
    public void OnDrawGizmos()
    {
        if(drawDebugEdges)
        {
            for (int i = 0; i < edges.Length; i++)
            {
                Vector3 startPosition = GetNode(edges[i].startNode).nodeObject.transform.position;
                Vector3 endPosition = GetNode(edges[i].endNode).nodeObject.transform.position;

                Gizmos.DrawLine(startPosition, endPosition);
            }
        }
    }

}
