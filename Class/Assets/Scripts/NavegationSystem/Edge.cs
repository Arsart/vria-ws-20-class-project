﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Edge
{
    public int startNode;
    public int endNode;

    public float speed = 1;

    private float distance;


    public float Weight
    {
        get =>  distance * speed;
    }

    public void SetDistance(float newValue)
    {
        distance = newValue;
    }



    public bool bidirectional = true;
}
