﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReproduceSound : MonoBehaviour
{
    public AudioClip[] clips;
    private AudioSource audioSource;
    private int clipCounter = 0;
    // Update is called once per frame

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            clipCounter++;
            if(clipCounter >= clips.Length)
                clipCounter = 0;
            audioSource.clip = clips[clipCounter];
            audioSource.Play();
        }
    }
}
